defmodule TaxiBeWeb.CustomerChannel do
  use TaxiBeWeb, :channel

  @impl true
  def join("customer:" <> _username, payload, socket) do
    {:ok, socket}
  end
end
